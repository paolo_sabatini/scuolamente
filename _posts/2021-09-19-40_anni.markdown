---
title:  "Più di quarantanni e far finta di niente."
date:   2021-09-19 08:00:00 +0100
layout: single

tagline: "BOZZA Le misure di igiene e sicurezza per la scuola dopo più di 40 anni delle loro emanazioni non sono ancora state recepite. Neanche ora che c'è il covid !"

header:
  teaser: /assets/images/2021-09-19-brutti-sporchi.jpg
  overlay_image: /assets/images/2021-09-19-brutti-sporchi.jpg
  overlay_filter: 0.5

categories: blog
tags : normative
---

Siamo nel pieno degli anni settanta, in Italia si vive anche in 
condizioni igieniche come quelle fotografate da Ettore Scola 
nel film "Brutti,sporchi e cattivi".
Ma un contesto del genere non impedisce al parlamento di interrogarsi 
su quali siano i requisiti minimi che devono rispettare le scuole per dirsi ambienti 
salubri e sicuri. 

Emana quindi una legge (n.412/1975) che prescrive, per mezzo di un decreto del Ministero per i Lavori 
Pubblici, le norme tecniche che le scuole costruite da allora avrebbero dovuto rispettare.

Il famoso (!?) Decreto Ministeriale 18 dicembre 1975 - "Norme tecniche 
aggiornate relative all'edilizia scolastica, ivi compresi gli indici 
minimi di funzionalità didattica, edilizia ed urbanistica da osservarsi 
nella esecuzione di opere di edilizia scolastica"

Ogni ministro, architetto, dirigente, ispettore, insegnante, genitore e 
anche alunno se di scuola vuole parlare dovrebbe conoscerlo!!

Ebbene nel 1975 per "funzionalità didattica" igiene e salute si prevede 
che in un aula scolastica debba esserci un indice di affollamento pari a:

- mq 1,96 per alunno (alle superiori)
- mq 1,80 per alunno negli altri cicli scolastici ;

non solo ma si prevede che per quanto riguarda l'aerazione ci debba essere
un coefficente di ricambio d'aria:

- pari almeno a 3,5 h (cioè in un'ora l'aria all'interno delle aule deve 
essere rinnovata totalmente 3,5 volte )

Sembrano altri tempi, non quelli attuali !!!

Ma dalle voci che mi arrivano le aule scolastiche sono sovraffollate, areate
alla green aprendo le finestre "quanto basta" sembrano tempi ancora da venire.  

È già. perchè da quando è stato sdoganato dal CTS il concetto che laddove non è 
possibile distanziare gli alunni si impone l'obbligo della mascherina 
chirurgica ( non di stoffa per intenderci), si è dato via ad un super affollamento.

Deus ex maschera!

Che poi ad onor del vero il CTS lo aveva detto anche lo scorso anno, ma lo 
scorso anno sulla scuola ha pesato il capriccio di Salvini che: 
"io a scuola mia figlia con la mascherina non ce la mando". 
Per non parlare della voglia di svecchiare le scuole con le sedie a rotelle:
 l'uso della mascherina avrebbe "vainficato" lo sforzo di acquistarle ...
e le mascherine a scuola non sono state usate sino al 2 dicembre ....
 
Ora abbiamo banchi singoli, o in alcuni bibosto segati, che vengono di nuovo affiancati ...

Siamo tutti contenti ! 

Siamo tornati alla normalità, compagni di banco, banchi a ridosso delle 
ante battenti delle finestre, e gli obbiettivi di contenimento ricondotti 
nell'ambito del solito refrain : meno classi, più alunni!!

Non so quale logica surreale faccia concludere che se non si riesce a distanziare linearmente allora si possa non rispettare una densità in superficie ..

Ahh la matematica, logica e geometria ora capisco perchè non la fanno insegnare come si deve....
 
Insomma quale astruso ragionamento possa far ritenere che il DM 18 dicembre 1975 ( 2 mq a studente) sia superato?

La consuetudine forse!! Si perché non è che non lo si rispetta da oggi, a giudicare dalle numerose sentenze l'illegalità è stata e continua ad essere la norma!!

Si capisce anche perché: la Gelmini (ultima in senso cronologico ad aumentare gli alunni per classe) come i precedenti e susseguenti Ministri dell'Istruzione hanno fatto finta di non sapere di quale metratura fossero le aule.
Le regioni, provincie, comuni hanno fatto finta di non sapere quanti alunnni ci fossero nelle classi delle loro scuole.
I dirigenti scolastici hanno fatto finta di non saper usare un metro ...
Le asl non controllavano ...

E gli insegnanti? Non pervenuti ...


Eppure basta aver coraggio di rompere questa situazione di stallo 
Non credo che questo dirigente che ottiene per due anni di seguito classi da venti alunni, si sia illuminato all'improvviso. 
Penso più che abbia avuto il fiato sul collo del RLS, di docenti, famiglie, Consisiglio di Istituto e collegio docenti innformati e decisi a far valere i loro diritti ..

E tutti gli altri ?
Non vedono l'ora di tornare alla normalità evidentemente, solo che la normalità era il problema..


TO DO:
inserire vari link .... 


https://www.aslroma6.it/dipartimenti-sanitari/dipartimento-di-prevenzione/servizio-igiene-e-sanita-pubblica/attivita


https://www.aslroma6.it/documents/20143/61401/00+-+D.M.+18-12-1975.pdf

